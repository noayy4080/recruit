<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $fillable = ['name','email'];

    //הקשר בין קנדידדט ליוזר הוא אונר
    public function owner(){
        //לכל קנדידט יכול להיות רק יוזר אחד
        //belognsTo = one to many
        return $this->belongsTo('App\User','user_id');
    }

    public function status(){
        return $this->belongsTo('App\Status','status_id');
    }

}


