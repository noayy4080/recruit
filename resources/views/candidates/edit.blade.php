@extends('layouts.app') 

@section('title','edit candidate')

@section('content')           
        <h1>Edit candidate</h1>
        <!--צריך לשלוח את הנתונים לפונקציה אפדייט יחד עם האיי.די-->
        <form method = "post" action = "{{action('CandidatesController@update',$candidate->id)}}">
        @method('PATCH')
        @csrf 
        <div class="form-group"> 
            <label for = "name">Candiadte name</label>
            <!--הואליו מציג את השם הנוכחי-->
            <input type = "text" class="form-control" name = "name" value = {{$candidate->name}}>
        </div>     
        <div class="form-group"> 
            <label for = "email">Candiadte email</label>
            <input type = "text" class="form-control" name = "email" value = {{$candidate->email}}>
        </div> 
        <div>
            <input type = "submit"  name = "submit" value = "Update candidate">
        </div>                       
        </form>    
@endsection
