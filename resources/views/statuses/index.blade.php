@extends('layouts.app')

@section('title', 'Candidates')

@section('content')

                    <h1>list of statuses</h1>
                    <table class = "table table-dark">
                        <tr>
                            <th>id</th>
                            <th>name</th>
                            <th>created</th>
                            <th>updated</th>
                        </tr>
                    @foreach($statuses as $status)
                        <tr>
                        <td>{{$status->id}}</td>
                        <td>{{$status->name}}</td>
                        <td>{{$status->created_at}}</td>
                        <td>{{$status->updated_at}}</td>
                        </tr>
                    @endforeach
                    </table>
@endsection
