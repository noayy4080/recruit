<?php

use Illuminate\Database\Seeder;

class Status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            'name' => 'before interview',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);        
    }
}

